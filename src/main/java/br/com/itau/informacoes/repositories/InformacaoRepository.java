package br.com.itau.informacoes.repositories;

import br.com.itau.informacoes.models.Contato;
import org.springframework.data.repository.CrudRepository;

public interface InformacaoRepository extends CrudRepository<Contato, Integer> {

    Iterable<Contato> findAllByUsuarioId(Integer usuarioId);
}
