package br.com.itau.informacoes.clients;

import br.com.itau.informacoes.clients.Cep;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class EnderecoClientFallback implements EnderecoClient {

    @Override
    public Cep consultarCep(String cep) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço VIACEP indisponível no momento. Tente novamente mais tarde.");
    }
}
