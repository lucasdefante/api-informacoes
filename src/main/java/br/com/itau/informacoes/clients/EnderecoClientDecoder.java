package br.com.itau.informacoes.clients;

import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class EnderecoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 400) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, response.reason());
        }
        return errorDecoder.decode(s, response);
    }
}
