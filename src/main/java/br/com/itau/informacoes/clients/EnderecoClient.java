package br.com.itau.informacoes.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "endereco", configuration = EnderecoClientConfiguration.class)
public interface EnderecoClient {

    @PostMapping("endereco/{numCep}")
    Cep consultarCep(@PathVariable String numCep);
}
