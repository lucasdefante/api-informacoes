package br.com.itau.informacoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class InformacoesApplication {

	public static void main(String[] args) {
		SpringApplication.run(InformacoesApplication.class, args);
	}

}
