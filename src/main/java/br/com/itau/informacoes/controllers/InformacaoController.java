package br.com.itau.informacoes.controllers;

import br.com.itau.informacoes.clients.Cep;
import br.com.itau.informacoes.clients.EnderecoClient;
import br.com.itau.informacoes.models.Contato;
import br.com.itau.informacoes.security.Usuario;
import br.com.itau.informacoes.services.InformacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
public class InformacaoController {

    @Autowired
    private InformacaoService informacaoService;

    @Autowired
    private EnderecoClient enderecoClient;

    @GetMapping("/info")
    public Usuario getUsuario(@AuthenticationPrincipal Usuario usuario) {
        System.out.println(usuario.toString());
        return usuario;
    }

    @PostMapping("/contato")
    public Contato criarContato(@AuthenticationPrincipal Usuario usuario, @RequestBody Contato contato) {
        contato.setUsuarioId(usuario.getId());
        Cep endereco = enderecoClient.consultarCep(contato.getCep());
        contato.setLogradouro(endereco.getLogradouro());
        return informacaoService.salvarContato(contato);
    }

    @GetMapping("/contatos")
    public Iterable<Contato> exibirContatos(@AuthenticationPrincipal Usuario usuario) {
        return informacaoService.getContatosPorUsuarioId(usuario.getId());
    }
}
