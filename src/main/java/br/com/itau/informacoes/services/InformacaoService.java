package br.com.itau.informacoes.services;

import br.com.itau.informacoes.models.Contato;
import br.com.itau.informacoes.repositories.InformacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InformacaoService {

    @Autowired
    private InformacaoRepository informacaoRepository;

    public Contato salvarContato(Contato contato) {
        return informacaoRepository.save(contato);
    }

    public Iterable<Contato> getContatosPorUsuarioId(Integer usuarioId) {
        return informacaoRepository.findAllByUsuarioId(usuarioId);
    }
}
